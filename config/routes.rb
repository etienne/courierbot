Rails.application.routes.draw do
  devise_for :admins, skip: [:sessions]
  devise_scope :admin do
    get 'login', to: 'devise/sessions#new', as: :new_admin_session
    post 'login', to: 'devise/sessions#create', as: :admin_session
    delete 'logout', to: 'devise/sessions#destroy', as: :destroy_admin_session
  end

  post 'sms/receive'
  resources :users

  root 'home#index'
  get 'home/index'
end
